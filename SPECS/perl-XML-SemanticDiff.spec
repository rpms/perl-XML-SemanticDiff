Name:		perl-XML-SemanticDiff
Summary:	Perl extension for comparing XML documents
Version:	1.0006
Release:	2%{?dist}
License:	GPL+ or Artistic
URL:		http://search.cpan.org/dist/XML-SemanticDiff/
Source0:	http://search.cpan.org/CPAN/authors/id/S/SH/SHLOMIF/XML-SemanticDiff-%{version}.tar.gz
BuildArch:	noarch
# Module Build
BuildRequires:	coreutils
BuildRequires:	perl-generators
BuildRequires:	perl%{?fedora:-interpreter} >= 4:5.12.0
BuildRequires:	perl(Module::Build) >= 0.28
# Module Runtime
BuildRequires:	perl(Digest::MD5)
BuildRequires:	perl(Encode)
BuildRequires:	perl(strict)
BuildRequires:	perl(warnings)
BuildRequires:	perl(XML::Parser)
# Test Suite
BuildRequires:	perl(blib)
BuildRequires:	perl(File::Spec)
BuildRequires:	perl(IO::Handle)
BuildRequires:	perl(IPC::Open3)
BuildRequires:	perl(Test)
BuildRequires:	perl(Test::More)
BuildRequires:	perl(vars)
# Optional Tests
BuildRequires:	perl(Test::TrailingSpace)
# Runtime
Requires:	perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version))

%description
XML::SemanticDiff provides a way to compare the contents and structure of two
XML documents. By default, it returns a list of hashrefs where each hashref
describes a single difference between the two docs.

%prep
%setup -q -n XML-SemanticDiff-%{version}

%build
perl Build.PL --installdirs=vendor
./Build

%install
./Build install --destdir=%{buildroot} --create_packlist=0
%{_fixperms} -c %{buildroot}

%check
./Build test

%files
%if 0%{?_licensedir:1}
%license LICENSE
%else
%doc LICENSE
%endif
%doc Changes eg/ README
%{perl_vendorlib}/XML/
%{_mandir}/man3/XML::SemanticDiff.3*
%{_mandir}/man3/XML::SemanticDiff::BasicHandler.3*

%changelog
* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.0006-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Sep 28 2017 Paul Howarth <paul@city-fan.org> - 1.0006-1
- Update to 1.0006
  - Fix failing to find the difference in this scenario (CPAN RT#84546):
    Before: <element>0</element>
    After: <element></element>
- Drop EL-5 support:
  - Drop BuildRoot: and Group: tags
  - Drop explicit buildroot cleaning in %%install section
  - Drop explicit %%clean section

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.0005-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Mon Jun 05 2017 Jitka Plesnikova <jplesnik@redhat.com> - 1.0005-2
- Perl 5.26 rebuild

* Wed Feb  8 2017 Paul Howarth <paul@city-fan.org> - 1.0005-1
- Update to 1.0005
  - Convert the distribution to use git, GitHub, and Dist-Zilla
  - Correct some spelling errors and add more tests
  - Remove trailing whitespace

* Mon May 16 2016 Jitka Plesnikova <jplesnik@redhat.com> - 1.0004-6
- Perl 5.24 rebuild

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.0004-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0004-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Jun 06 2015 Jitka Plesnikova <jplesnik@redhat.com> - 1.0004-3
- Perl 5.22 rebuild

* Mon Oct  6 2014 Paul Howarth <paul@city-fan.org> - 1.0004-2
- Incorporate feedback from package review (#1148577)
  - Fix typo in %%description
  - Package eg/ as documentation

* Wed Oct  1 2014 Paul Howarth <paul@city-fan.org> - 1.0004-1
- Initial RPM version
